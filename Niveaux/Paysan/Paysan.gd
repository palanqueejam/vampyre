extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player
var life_count = 6

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_tree().get_root().find_node("Joueur", true, false)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null:
		look_at(Vector2(player.position.x, position.y))
		if player.position.x - position.x < 0:
			$PaysanFocus.flip_v = true
			$PaysanIdle.flip_v = true
		else:
			$PaysanFocus.flip_v = false
			$PaysanIdle.flip_v = false


func _on_HitBox_area_entered(area):
	if area.is_in_group("hurtbox_player"):
		take_hit()
		
		
func take_hit():
	print(self, " - take hit")
	if $AnimationPlayer.current_animation == "Took_Hit" and $AnimationPlayer.is_playing():
		return
	
	$AnimationPlayer.play("Took_Hit")
	life_count -= 1
	print(self, " - life_count: ", life_count)
	if life_count == 0:
		pass
		#code to load next scene
	elif life_count < 3:
		player.take_knockback()
	elif life_count < 4:
		pass
		#switch to 2nd phase ##(if not already)
		$AnimationPlayer.play("Focus_Start")
		var poule = get_tree().get_root().find_node("Poule", true, false)
		if poule != null:
			poule.visible = true

func _on_AnimationPlayer_animation_finished(anim):
	if anim == "Focus_Start":
		$AnimationPlayer.play("Focus_Loop")
		$AnimationPlayer.queue("Idle")
