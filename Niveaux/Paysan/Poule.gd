extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player
var target_player = false
var return_from_target_player = false
var target_position
var original_pos

var deltasum = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_tree().get_root().find_node("Joueur", true, false)
	original_pos = global_position
	visible = false
	$HurtBox/CollisionShape2D.disabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null and visible:
		$HurtBox/CollisionShape2D.disabled = false
		look_at(player.position)
		if fposmod(rotation_degrees + 90, 360) < 180:
			$AnimatedSprite.flip_v = false
			#print(self, "flipping_h")
		else:
			$AnimatedSprite.flip_v = true
		#print(self, rotation_degrees, " ", fposmod(rotation_degrees + 90, 360))
		if target_player:
#			lerp(global_position, player.global_position, 0.7 * delta)
			global_position = lerp(global_position, target_position, 0.7 * delta)
			#print(self, " - target_player ", global_position, target_position, (global_position - target_position).length())
			if abs((global_position - target_position).length()) < 25:
				global_position = target_position
			elif abs((global_position - target_position).length()) < 2:#global_position == target_position:
				return_from_target_player = true
				target_player = false
		elif return_from_target_player:
			global_position = lerp(global_position, original_pos, 0.7 * delta)
			if abs((global_position - original_pos).length()) < 25:
				global_position = original_pos
				return_from_target_player = false
		
		if deltasum > 2:#11:
			deltasum = 0
			trigger_target_player()
		deltasum += delta

func trigger_target_player():
	if return_from_target_player:
		return
		
	target_player = true
	target_position = player.global_position
	original_pos = global_position
