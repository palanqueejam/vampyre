extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVEMENT_SPD = 300#150
const FALLING_MAXSPD = 680 #640#600#380 #310
const FALLING_BASESPD = 170 #100#20#220
const JUMP_POWER = 2100.0#3000.0#1000.0#260.0#60.0
const LIFE_COUNT_MAX = 3

var falling = 0.0
var jumping = false
var gliding = false
var life_count

# Called when the node enters the scene tree for the first time.
func _ready():
	life_count = LIFE_COUNT_MAX
	reset()


func _process(delta):
	process_action_input(delta)
	
	if position.y > 2000:
		get_tree().reload_current_scene()
		#ideally respawn to a Position2D point placed and referenceable thanks to a nodepath


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	process_movement_input(delta)


func process_movement_input(delta):
	var move = Vector2.ZERO
#	if Input.is_action_pressed("ui_left"):
#		move.x -= 1
#	if Input.is_action_pressed("ui_right"):
#		move.x += 1
#	if Input.is_action_pressed("ui_up"):
#		move.y -= 1
#	if Input.is_action_pressed("ui_down"):
#		move.y += 1

	if Input.is_action_pressed("ui_left"):
		move.x -= MOVEMENT_SPD
	if Input.is_action_pressed("ui_right"):
		move.x += MOVEMENT_SPD
	if Input.is_action_pressed("ui_accept") and is_on_floor() and !gliding:
		trigger_jump()
		
	if is_jumping():
		falling = lerp(falling, 0, 10*delta)
		if falling > -18:
			jumping = false
			falling = 0
			if Input.is_action_pressed("ui_accept"):
				gliding = true
	elif is_gliding():
		falling = lerp(falling, FALLING_MAXSPD, 0.05 * delta)
#		if Input.is_action_just_released("ui_accept"):
		if !Input.is_action_pressed("ui_accept"):
			gliding = false
			if falling < FALLING_BASESPD:
				falling = FALLING_BASESPD
			else:
				falling = lerp(falling, FALLING_MAXSPD, 0.4 * delta)
	elif is_on_floor():
		falling = 0
	elif falling == 0: #not onfloor and falling == 0
		falling = FALLING_BASESPD
	else:
		falling = lerp(falling, FALLING_MAXSPD, 0.4 * delta)
		#falling = move_toward(falling, FALLING_MAXSPD, 40 * delta)
	#move.y += 220
	move.y = falling
	
#	self.state = State.MOVE #uncomment when move state is implemented (move state = animation + can process attack)
	#move *= MOVEMENT_SPD * delta
	
	var direction = sign(move.x)
	if direction != 0:
#		scale.x = direction * abs(scale.x)
#		print(scale.x)
		scale.y = direction * abs(scale.y)
		rotation_degrees = 180 / 2 * (1 - direction)
		for c in get_children():
			if c is Camera2D:
				c.scale.x = direction * abs(c.scale.x)
		
	#position += move
	#move_and_collide(move) #move_and_collide doesn't take into account the global_scale of the current object
	move_and_slide(move, Vector2.UP)
	
	
func trigger_jump():
	jumping = true
	falling = -JUMP_POWER


func is_jumping():
	return jumping


func is_gliding():
	return gliding


func reset():
	jumping = false


func process_action_input(delta):
	if Input.is_action_just_pressed("punch"):
		trigger_attack()
		
		
func trigger_attack():
	$AnimationPlayer.play("Attack")
	$AnimationPlayer.queue("RESET")
	#change animation depending on level
	
	
func take_hit():
	if $AnimationPlayer.current_animation == "Took_Hit" and $AnimationPlayer.is_playing():
		return
	
	$AnimationPlayer.play("Took_Hit")
	life_count -= 1
	if life_count < 1:
		get_tree().reload_current_scene()


#When an enemy is angered and you get knockbacked after hitting them
func take_knockback():
	pass
	#tocode

func _on_HitBox_area_entered(area):
	if area.is_in_group("hurtbox_enemy"):
		take_hit()
