extends Area2D
class_name DetectionBox


enum BoxType {HURT = 0, HIT = 1, GRAB = 2}
enum FactionType {HERO = 0, ENEMY = 1}

const BOXTYPESTR = ["hurtbox_", "hitbox_", "grabbox_"]
const FACTIONTYPESTR = ["player", "enemy"]

export(BoxType) var box_type
export(FactionType) var faction


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(BOXTYPESTR[box_type] + FACTIONTYPESTR[faction], true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
