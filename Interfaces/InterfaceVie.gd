extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var heart_shown = 3
#export(PackedScene) var player
export(NodePath) var player_np
var player = null

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node(player_np)
	if player != null:
		#heart_shown = player.life_count
		yield(get_tree(), "idle_frame")
		update_nb_heart_displayed()
	print(player)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null:
		#print(player.life_count)
		if heart_shown != player.life_count:
			update_nb_heart_displayed()

#Max heart displayable = 10
#Fixable by creating the heart in the code instead of in the scene
func update_nb_heart_displayed():
	heart_shown = player.life_count
	hide_all_heart()
	for i in heart_shown:
		$HBoxContainer.get_child(i).visible = true
		
func hide_all_heart():
	for c in $HBoxContainer.get_children():
		c.visible = false
