extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var wait_to_start = false
var deltasum = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Cercueil/AnimationPlayer.connect("animation_finished", self, "_on_anim_finished")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Cercueil/AnimationPlayer.play("cercueil")
	if wait_to_start:
		deltasum += delta
	if deltasum > 5:
		get_tree().change_scene("res://Niveaux/NiveauxAssembles.tscn")
	
func _on_anim_finished(anim):
	if anim == "cercueil":
		wait_to_start = true
